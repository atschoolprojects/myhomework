<?php
    session_start();

    //includes the Constants
    include 'codeConstants.php';
    $constants = NEW Constants();

    //show loading symbol
    echo '<div class="loading"></div>';

    //check if the session variable is set
    if ($_SESSION["UserID"] != null) {

        //check if cancel-button was clicked
        if (isset($_POST['cancel'])) {
            //return to overview.php
            header("Location: overview.php");
        }
        else {
             //connect to the database
             $pdo = connectDatabase();

                $statement = $pdo->prepare("SELECT * FROM `entry`");
                $statement->execute(array()); 
                
                //Checks if th requestmethod is post
                if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST))
                {
                    //initialise the variables
                    $entryID = intval($_POST["entryID"]); //intval turns variable into int
                    $type = $_POST["type"];
                    $subject = $_POST["subject"];
                    $description = $_POST["description"];
                    
                    //change dateformat in case of the user typed in differently
                    $date = date_create($_POST["completionDate"]);
                    $completionDate = date_format($date,"Y-m-d");

                    //if entryID is not null the user wants to edit the entry
                    if ($entryID != null) {

                        $user = $pdo->prepare("SELECT UserID FROM entry WHERE EntryID = ?");
                        $user->execute(array($entryID));

                        //Get the userID from the editing user
                        $activeUserID = $user->fetch()[0];

                        //if the editing user is the logged in user the entry become updated
                        if ($activeUserID == $_SESSION["UserID"]) {
                            $editedEntry = $pdo->prepare("UPDATE entry SET Type= ?, Subject= ?, CompletionDate= ?, Description= ? WHERE EntryID= ?");
                            $editedEntry->execute(array($type, $subject, $completionDate, $description, $entryID)); 
                        }
                        else {
                            header("Location: overview.php");
                        }

                       
                    }
                    else {
                        $entry = $pdo->prepare("INSERT INTO entry (UserID, Type, Subject, Description, CompletionDate)
                        VALUES (?, ?, ?, ?, ?)");
                        $entry->execute(array($_SESSION["UserID"], $type, $subject, $description, $completionDate));
                        
                    }

                    //returns to the overview page
                    header("Location: overview.php");
                } 
            } 
        }
     
     else {
        //The Sesseionvariable is not set
        header("Location: index.php");
     }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="CSS\style.css" />
    <title>Add Entry</title>
</head>
<body>
    
</body>
</html>