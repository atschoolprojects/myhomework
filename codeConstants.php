<?php
    class Constants{

        function writeHead()
        {
          //writes the Head so it is not nessesary to rewrite it
          echo '
          <!-- Tabcontent -->
          <link rel="shortcut icon" href="IMG\favicon.ico" />

          <!-- Meta -->
          <meta charset="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          
          <!-- CSS -->
          <link rel="stylesheet" href="CSS\style.css" />
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"/>
          <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>

          <!-- Script -->
          <script src="JAVASCRIPT/script.js"></script>
          <script src="https://kit.fontawesome.com/025b998bae.js" crossorigin="anonymous"></script>
          <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
          
          <script>
            //Navbar for mobile divices
            $(document).ready(function(){
              $(\'.sidenav\').sidenav();
            });

            //Charcounter at Signup password
            $(document).ready(function() {
              $(\'input#signUpPassword\').characterCounter();
            });

            //Selectbox
            $(document).ready(function(){
              $(\'select\').formSelect();
            });

            //Datepicker
            $(document).ready(function(){
              $(\'.datepicker\').datepicker({
                format: \'yyyy-mm-dd\'                
              });
            });   
          </script>
          ';          
        }

        function writeHeader(){
            //Writes the header so it is not nessesary to write it in every singel file
            echo '
            <header>
            <div class="navbar-fixed">
            <nav class="nav-wrapper">
              <a href="#" class="sidenav-trigger right" data-target="mobile-links"><i class="material-icons">menu</i></a>
              <a href="index.php" class="left hide-on-med-and-down"><i class="fas fa-home"></i></a>
              <a class="brand-logo center">MyHomework</a>
                <ul class="right hide-on-med-and-down">
                  <li><a href="signin.php">Sign in</a></li>
                  <li><a href="signup.php">Sign up</a></li>
                </ul>
            </nav>
          </div>
          
          <ul class="sidenav" id="mobile-links">
            <li><a href="index.php">Home</a></li>
            <li><a href="signin.php">Sign in</a></li>
            <li><a href="signup.php">Sign up</a></li>
          </ul>
            </header>';
        }

        function writeLoggedInHeader(){
          //Writes the Header when the User is logged in
          echo '
          <div class="navbar-fixed">
          <nav class="nav-wrapper">
            <a href="#" class="sidenav-trigger right" data-target="mobile-links"><i class="material-icons">menu</i></a>
            <a class="brand-logo center">MyHomework</a>
              <ul class="right hide-on-med-and-down">
                <li><a onclick="confirmBtnClick()">Sign out</a></li>
              </ul>
          </nav>
        </div>
        
        <ul class="sidenav" id="mobile-links">
          <li><a href="logout.php">Sign out</a></li>
        </ul>';
        }

        function writeFooter(){
            //Writes the footer so it is not nessesary to write it in every singel file
            echo '
            <footer class="page-footer">
              <div class="container">
                <div class="row">
                  <div class="col l6 s12">
                    <h5 class="white-text">Contact</h5>
                    <p class="grey-text text-lighten-4">
                      Manuel Gwerder<br />8934 Knonau<br />Schweiz
                    </p>
                    <p class="grey-text text-lighten-4">
                      <a href="mailto:mgwerder9@gmail.com"
                        ><i class="far fa-envelope">&nbsp;</i>mgwerder9@gmail.com</a
                      ><br /><i class="fas fa-phone">&nbsp;</i>+41 77 479 94 48
                    </p>
                  </div>
                  <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Social Media</h5>
                    <ul>
                      <li>
                        <a
                          class="grey-text text-lighten-3"
                          href="https://www.instagram.com/21___manuel___21/"
                          target="_blank"
                          ><i class="fab fa-instagram"></i> Instagram</a
                        >
                      </li>
                      <li>
                        <a
                          class="grey-text text-lighten-3"
                          href="https://twitter.com/intent/follow?screen_name=_another_manuel"
                          target="_blank"
                          ><i class="fab fa-twitter"></i> Twitter</a
                        >
                      </li>
                      <li>
                        <a
                          class="grey-text text-lighten-3"
                          href="https://gitlab.com/AnotherManuel"
                          target="_blank"
                          ><i class="fab fa-gitlab"></i> GitLab</a
                        >
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="footer-copyright">
                <div class="container">&copy; 2020 Copyright by Manuel Gwerder</div>
              </div>
            </footer>';
        }
    }

    function connectDatabase(){
      // return mysqli_connect("localhost","root","","myhomework");
      return new PDO('mysql:host=localhost;dbname=myhomework', 'root', '');
    }

    function disconnectDatabase($result, $connection){
      //frees the memory associated with the result
      mysqli_free_result($result);
      //closes the connection
      mysqli_close($connection);
    }

?>