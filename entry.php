<?php
  session_start();

  //includes the constants
  include 'codeConstants.php';
  $constants = NEW Constants();
  $constants -> writeHead();

  if ($_SESSION["UserID"] != null) {
    $pdo = connectDatabase();
      //Initialise variables
      $id = null;
      $type = "";
      $subject = "";
      $completionDate = "";
      $description = "";
      
      //If the entryID is set the user edits this entry
      if (isset($_GET['editID'])) {
          $id = $_GET['editID'];

          $statement = $pdo->prepare("SELECT * FROM `entry` WHERE UserID = ? AND EntryID = ?");
          $statement->execute(array($_SESSION["UserID"], $id)); 

          while($row = $statement->fetch()) {
            //fill in the values from the database
            $type = strip_tags($row["Type"]);
            $subject = strip_tags($row["Subject"]);
            $completionDate = strip_tags($row["CompletionDate"]);
            $description = strip_tags($row["Description"]);
                      
          }
      }
    }    
  
  else {
    //The Sesseionvariable is not set
    header("Location: index.php");
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Tabcontent -->
    <title>MyHomework - Entry</title>

  </head>
  <body>

  <?php
    $constants -> writeLoggedInHeader();
  ?>

  <div style="margin-top: 15vh"></div>

    <form action="addEntry.php" method="post">
      <div class="row">

        <!-- Type -->
        <div class="input-field col 11 m3">
          <select id="type" name="type" required>
            <option value="Homework">Homework</option>
            <!-- If the variable contains Exam this option is selectet. Otherwise the Homework by default -->
            <option <?php if ($type == "Exam") {echo 'selected';} ?> value="Exam">Exam</option>
          </select>
          <label for="type">Type</label>
        </div>

        <!-- Subject -->
        <div class="input-field col s11 m4 offset-m1">
          <!-- If $subject has a value it will be filled into the input-field -->
          <input id="subject" name="subject" type="text" class="validate" required value= "<?php echo $subject; ?>" />
          <label for="subject">Subject</label>
        </div>

        <!-- Completion date -->
        <div class="input-field col s11 m3 offset-m1">
          <!-- If $completionDate has a value it will be filled into the input-field -->
          <input id="completionDate" name="completionDate" type="text" class="datepicker" required value= "<?php echo $completionDate; ?>" >
          <label for="completionDate">Completion date</i></label>
        </div>

      </div>

      <div style="margin-top: 15vh"></div>
      <div class="row">
        
        <!-- Description -->
        <div class="input-field col s11 m12">
          <!-- If $description has a value it will be filled into the input-field -->
          <textarea id="description" name="description" required class="materialize-textarea"><?php echo $description; ?></textarea>
          <label for="description">Description</label>
        </div>

      </div>
      <div class="row">

        <!-- Cancel button -->
        <div class="col s3 m1 offset-s6 offset-m10">
          <button class="btn waves-effect waves-light red lighten-2" name="cancel"/>Cancel</button>
        </div>

        <!-- Save button -->
        <div class="col s3 m1">
          <button class="btn waves-effect waves-light red lighten-2" type="submit" name="action">Save</button>
        </div>

      </div>  
        <!-- Hidden entryID -->
        <input type="hidden" name="entryID" value= "<?php echo $id; ?>">
      
    </form>
  </body>
</html>
