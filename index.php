<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Tabcontent -->
    <title>MyHomework - Home</title>
   
   <!-- PHP -->
    <?php
      include 'codeConstants.php';
      $constants = NEW Constants();
      $constants -> writeHead();
    ?>

  </head>
  <body>

    <?php
      $constants -> writeHeader();
    ?>

    <!-- Title and description -->
    <main>
      <h4>-Your online Homeworkbook-</h4>

      <p id="introduction">
        MyHomework is your way to manage your homework and exams. You can
        create, edit or delete them. In an clear overwiev you can see all your
        entries and decide what to do next.
      </p>

      <!-- Advantages -->
      <br />
      <h4>Your advantages</h4>

      <div class="row">
        <div class="col m3">
          <div class="card">
            <div class="card-image">
              <img src="IMG\check.png" />
              <span
                class="card-title"
                style="width: 100%; background: rgba(0, 0, 0, 0.5)"
                >Simple</span
              >
            </div>
            <div class="card-content">
              <p>
                MyHomework is a very simple application. You don't need to be a
                professional internet user or computer scientist. All functions
                should be self-explanining. If you either have a question feel
                free to write an email.
              </p>
            </div>
          </div>
        </div>
        <div class="col m3">
          <div class="card">
            <div class="card-image">
              <img src="IMG\free.png" />
              <span
                class="card-title"
                style="width: 100%; background: rgba(0, 0, 0, 0.5)"
                >Free</span
              >
            </div>
            <div class="card-content">
              <p>
                MyHomework is free. There is no content that is only for
                subscribers or something like this. All functions are enabled
                for everyone. If you want to support me I would appreciate a
                little donation.
              </p>
            </div>
          </div>
        </div>
        <div class="col m3">
          <div class="card">
            <div class="card-image">
              <img src="IMG\secure.png" />
              <span
                class="card-title"
                style="width: 100%; background: rgba(0, 0, 0, 0.5)"
                >Secure</span
              >
            </div>
            <div class="card-content">
              <p>
                Your private data won't be published or shared by me. Everything
                will be stored secure and won't be visible for everyone else
                than you. But if a hacker wants to get your data, I decline any liability. 
              </p>
            </div>
          </div>
        </div>
        <div class="col m3">
          <div class="card">
            <div class="card-image">
              <img src="IMG\opensource.png" />
              <span
                class="card-title"
                style="width: 100%; background: rgba(0, 0, 0, 0.5)"
                >Open Source</span
              >
            </div>
            <div class="card-content">
              <p>
                For all programmers out there who want to check if the code is
                fine: it's open source and a link to it is
                <a
                  href="https://gitlab.com/atschoolprojects/myhomework"
                  target="_blank"
                  rel="noopener noreferrer"
                  >here</a
                >. Feel free to add your own functions and stuff. If you have
                found a bug, I would be thankful if you could contact me.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s8 offset-s2">
          <div class="card white">
            <div class="card-content white-text center">
              <img src="IMG/logo.png">
            </div>
            <div class="card-action">
              <a href="signup.php">Get started</a>
            </div>
          </div>
        </div>
      </div>
    </main>
    <?php
      $constants -> writeFooter();
    ?>    
  </body>
</html>