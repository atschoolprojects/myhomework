<?php
  //destroys the session and link to the index.php
  session_start();
  session_destroy();

  //link to the index site
  header("Location: index.php");
?>