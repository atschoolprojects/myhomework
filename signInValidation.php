<?php
    session_start();

    include 'codeConstants.php';
    $constants = NEW Constants();

    //show loading symbol
    echo '<div class="loading"></div>';

    //connect to the database
    $pdo = connectDatabase();

     
        //  $sql = "SELECT * FROM user";
        //  $result = mysqli_query($connection,$sql);

        $statement = $pdo->prepare("SELECT * FROM user");
        $statement->execute(array()); 

         $isInputValid = false;

             if ($statement->rowCount() > 0) {
               
                //for every row in table
                while($row = $statement->fetch()) {
                         
                    //checks if the inputs are valid
                    if ($_POST['signInUsername'] == strip_tags($row["Username"]) && password_verify($_POST['signInPassword'], strip_tags($row["Password"]))) {
                        //input was valid
                        $isInputValid = true;
                        $_SESSION["UserID"] = strip_tags($row["UserID"]);
                        header("Location: overview.php");
                    }                  
                }
            }    
            if (!$isInputValid) {
                // input was invalid
                header("Location: signin.php");
            }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="CSS\style.css" />
        <title>Validating</title>
    </head>
    <body>
        
    </body>
</html>