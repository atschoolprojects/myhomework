<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Tabcontent -->
    <title>MyHomework-Sign In</title>

    <!-- PHP -->
    <?php
      include 'codeConstants.php';
      $constants = NEW Constants();
      $constants -> writeHead();
    ?>

  </head>
  <body onload='validateLastLocation("signin.php")'>
    
    <?php
      $constants -> writeHeader();
    ?>

    <main>
      <form action="signInValidation.php" method="post">
        <h1>Sign in</h1>

        <!-- Space from top -->
        <div style="margin-top: 15vh"></div>

        <!-- Username -->
        <div class="row">
          <div class="input-field col s8 xl6 offset-s2 offset-xl3">
            <input id="signInUsername" name="signInUsername" type="text" required class="validate" />
            <label for="signInUsername">Username</label>
          </div>
        </div>

        <!-- Password -->
        <div class="row">
          <div class="input-field col s8 xl6 offset-s2 offset-xl3">
            <input id="signInPassword" name="signInPassword" type="password" required class="validate" />
            <label for="signInPassword">Password</label>
          </div>
        </div>

        <!-- Link to signup.php -->
        <div class="row">
          <div class="center">
            <p class="center-align">
              <a href="signup.php" style="text-align: right">Don't have an account? Sign up <i class="fas fa-arrow-right"></i></a>
            </p>
          </div>
        </div>

        <!-- Submit button -->
        <div class="row">
          <div class="col s1 offset-s5 offset-m7"></div>
          <button class="btn waves-effect waves-light red lighten-2" type="submit" name="action">
            Sign in<i class="material-icons right">send</i>
          </button>
        </div>
      <form>
    </main>
  </body>
</html>
