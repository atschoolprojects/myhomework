<!DOCTYPE html>
<html lang="en">
  <head>
    
    <!-- Tabcontent -->
    <title>MyHomework-Sign In</title>

    <!-- PHP -->
    <?php
      include 'codeConstants.php';
      $constants = NEW Constants();
      $constants -> writeHead();
    ?>
    
  </head>
  <body onload='validateLastLocation("signup.php")'>

  <?php
    $constants -> writeHeader();
  ?>

    <main>      
      <form action="signUpValidation.php" method="post">
        <h1>Sign up</h1>

        <!-- Space to top -->
        <div style="margin-top: 15vh"></div>

        <!-- Username -->
        <div class="row">
          <div class="input-field col s8 xl6 offset-s2 offset-xl3">
            <input id="signUpUsername" name="signUpUsername" type="text" class="validate" />
            <label for="signUpUsername">Username</label>
          </div>
        </div>

        <!-- Password -->
        <div class="row">
          <div class="input-field col s8 xl6 offset-s2 offset-xl3">
            <input id="signUpPassword" name="signUpPassword" type="password" data-length="8" class="validate" />
            <label for="signUpPassword">Password</label>
          </div>
        </div>

        <!-- Link to signin.php -->
        <div class="row">
          <div class="center">
            <p class="center-align">
              <a href="signin.php" style="text-align: right">Already signed up? Sign in <i class="fas fa-arrow-right"></i></a>
            </p>
          </div>
        </div>

        <!-- Submit button -->
        <div class="row">
          <div class="col s1 offset-s5 offset-m7"></div>
          <button class="btn waves-effect waves-light red lighten-2" type="submit" name="action">
            Sign up <i class="material-icons right">send</i>
          </button>
        </div>
        
      </form>
    </main>
  </body>
</html>
